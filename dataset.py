import numpy as np
import torch

class MyDataset(torch.utils.data.Dataset):
    
    def __init__(self, X, sr,sample_seconds):
        self.sample_seconds = sample_seconds
        self.X = X
        self.sr = sr
        
    def __getitem__(self, index):
        
        song = self.X[index]
             
        amount_sample_frames = self.sr[index] * self.sample_seconds
        song_length = len(song)
        
        max_range = song_length - amount_sample_frames
        start = np.random.randint(0, max_range-1)
        
        random_slice = song[start:start+amount_sample_frames]
        
        random_slice = torch.FloatTensor(random_slice)
        
        random_slice = random_slice.unsqueeze(1)
        
        return random_slice # [song_length, song_features]
    
    def __len__(self):
        return len(self.X) 