# Neural Network - Cochlear Implant and Music

This repository contains the implementation of the temporal convolutional neural network beloning to the thesis "Electrical sounds: pre-processed music for cochlear implant users". 

## References: Data
The data used for this implementation is the dataset from MusicNet:
- MusicNet: https://homes.cs.washington.edu/~thickstn/musicnet.html 

In order to generate the .sav file, download the raw audio files form MusicNet and run data_save.py. This program saves all audio files efficiently as Numpy arrays using Librosa's load function and Klepto's archive function.

## References: Code 
Note that this implementation uses code from the following users:
- Floris Laporte: PyTorch lfilter. Link: https://github.com/flaport/torch_lfilter 
- Alexandre Chabot-Leclerc: Vocoder. Link: https://github.com/achabotl/vocoder
- Shaojie Bai, J. Zico Kolter and Vladlen Koltun: Temporal Convolutional Network. Link: https://github.com/locuslab/TCN 

The licenses from these users still apply. All other code written by myself is under the MIT license, which can be found here: [License](https://gitlab.socsci.ru.nl/L.Grootjen/neural-network-cochlear-implant-and-music/-/blob/master/LICENSE)

### Changes to Vocoder by Alexandre Chabot-Leclerc:
The following changes have been made to his implementation of the Vocoder:
- The implementation can deal with PyTorch tensors now. This means all Numpy functions are replaced by PyTorch functions.
- The implementation can deal with batches now. 
- The implementation offers the option to add noise (instead of adding it by default).
- The implementation offers the option to amplify frequencies which are not filtered out by the filterbank.
