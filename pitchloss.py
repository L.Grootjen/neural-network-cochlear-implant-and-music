import torchaudio

class PitchLoss():
    
    def __init__(self, vocoder):
        self.vocoder = vocoder

    def calc_voc_spec(self, outputs, x, fs):
        
        flat_x = x.squeeze(2)
        flat_outputs = outputs.squeeze(2)
    
        filtered_x = self.vocoder.vocode_batch(flat_x, fs, True, False)
        filtered_outputs = self.vocoder.vocode_batch(flat_outputs, fs, False, False)
    
  
        transformer = torchaudio.transforms.Spectrogram()
        
        spec_x = transformer(filtered_x)
        spec_out = transformer(filtered_outputs)
        
        return spec_out.double(), spec_x.double()

        