import joblib
import numpy as np
import librosa
import torch
import soundfile as sf
from model import TCN
from noisevocoder import NoiseVocoder
from klepto.archives import file_archive
from dataset import MyDataset


#### HELPER FUNCTIONS ####
def tf_to_np(tensor_song, device):   
    if device.type == 'cuda':
        tensor_song = tensor_song.cpu()
    numpy_song = tensor_song.detach().numpy().ravel()
    return numpy_song

def check_resonants(song, fs):
    pitches, magnitudes = librosa.piptrack(y=song, sr=fs)
    amount_x_resonants = np.array([])
    
    time = pitches.shape[1]
    
    for t in range(0, time):
        x_resonants = resonant_at_t(pitches[:,t], magnitudes[:,t])
        amount_x_resonants = np.append(amount_x_resonants, x_resonants)
    
    return np.sum(amount_x_resonants)
    

def resonant_at_t(pitch_array, magnitude_array, margin_of_error = 0.01):
    fundamental_tone_index = np.argmax(magnitude_array)
    fundamental_tone = pitch_array[fundamental_tone_index]

    sorted_amplitudes = np.argsort(magnitude_array) #from small to large
    reversed_amplitudes = sorted_amplitudes[::-1] # from large to small

    resonants = 0
    resonant = fundamental_tone

    for i in range(0, len(reversed_amplitudes)):
        theoretical_resonant = resonant + fundamental_tone
        practical_resonant = pitch_array[reversed_amplitudes[i]]
        lowest_range = theoretical_resonant * (1 - margin_of_error)
        highest_range = theoretical_resonant * (1 + margin_of_error)
        if lowest_range <= practical_resonant <= highest_range:
            resonants+=1

    return resonants

def test_model(model, data, device, vocoder, random_sampling = 1):
    test_loader = data["test"]
    test_sr = data["test_sr"]
    
    original_songs = []
    optimised_songs = []
    original_songs_noise = []
    optimised_songs_noise = []
    srs = []
    
    for sample in range(0, random_sampling):
        for (song, sr) in zip(test_loader, test_sr): 
            X_test = song.to(device)

            predicted = model.forward(X_test)
            flat_x = X_test.squeeze(2)
            flat_outputs = predicted.squeeze(2)

            ## Vocode with noise
            vocoded_x_noise = vocoder.vocode_batch(flat_x, sr, False, True)
            vocoded_predicted_noise = vocoder.vocode_batch(flat_outputs, sr, False, True)

            np_x_test_noise = tf_to_np(vocoded_x_noise, device)
            np_predicted_noise = tf_to_np(vocoded_predicted_noise, device)

            original_songs_noise.append(np_x_test_noise)
            optimised_songs_noise.append(np_predicted_noise)
            
            ## Vocode without noise
            vocoded_x = vocoder.vocode_batch(flat_x, sr, False, False)
            vocoded_predicted = vocoder.vocode_batch(flat_outputs, sr, False, False)

            np_x_test = tf_to_np(vocoded_x, device)
            np_predicted = tf_to_np(vocoded_predicted, device)

            original_songs.append(np_x_test)
            optimised_songs.append(np_predicted)
            
            srs.append(sr)

    return original_songs_noise, optimised_songs_noise, original_songs, optimised_songs, srs


#### LOAD IN DATA ####

data = {}

filename = "music_data"
loaded_model = file_archive(filename)
loaded_model.load()

data["train"] = torch.utils.data.DataLoader(MyDataset(loaded_model["train"], loaded_model["train_sr"], 8), batch_size=5)
data["valid"] = torch.utils.data.DataLoader(MyDataset(loaded_model["valid"], loaded_model["valid_sr"], 8), batch_size=10)
data["test"] = torch.utils.data.DataLoader(MyDataset(loaded_model["test"], loaded_model["test_sr"], 8), batch_size=1)

data["train_sr"] = loaded_model["train_sr"]
data["test_sr"] = loaded_model["test_sr"]
data["valid_sr"] = loaded_model["valid_sr"]


#### TEST MODEL ####

tcn = TCN(1, [8, 16, 16, 8], 1, dropout=0.1)
tcn.load_state_dict(torch.load("model8-16-16-8-drop01.pt", map_location=torch.device('cpu')))
tcn.eval()

center_frequencies = [250, 500, 1000, 2000, 4000, 8000]
vocoder = NoiseVocoder(center_frequencies)
#device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = torch.device('cpu')

original_songs_noise, optimised_songs_noise, original_songs, optimised_songs, srs = test_model(tcn, data, device, vocoder)

resonants_original_noise = np.array([])
resonants_optimised_noise = np.array([])
resonants_original = np.array([])
resonants_optimised = np.array([])

for i, (original_song_noise, optimised_song_noise) in enumerate(zip(original_songs_noise, optimised_songs_noise)) :
    sr = srs[i]
    resonants_original_noise = np.append(resonants_original_noise, check_resonants(original_song_noise, sr))
    resonants_optimised_noise = np.append(resonants_optimised_noise, check_resonants(optimised_song_noise, sr))
    
    filename_original = './results/with_noise/wav/original_song{}.wav'.format(i)
    filename_optimised = './results/with_noise/wav/optimised_song{}.wav'.format(i)
    sf.write(filename_original, original_song_noise, sr)
    sf.write(filename_optimised, optimised_song_noise, sr)

for i, (original_song, optimised_song) in enumerate(zip(original_songs, optimised_songs)) :
    sr = srs[i]
    resonants_original = np.append(resonants_original, check_resonants(original_song, sr))
    resonants_optimised = np.append(resonants_optimised, check_resonants(optimised_song, sr))
    
    filename_original = './results/without_noise/wav/original_song{}.wav'.format(i)
    filename_optimised = './results/without_noise/wav/optimised_song{}.wav'.format(i)
    sf.write(filename_original, original_song, sr)
    sf.write(filename_optimised, optimised_song, sr)
    
joblib.dump(resonants_original_noise, "resonants_original_noise.sav")
joblib.dump(resonants_optimised_noise, "resonants_optimised_noise.sav")
joblib.dump(resonants_original, "resonants_original.sav")
joblib.dump(resonants_optimised, "resonants_optimised.sav")




