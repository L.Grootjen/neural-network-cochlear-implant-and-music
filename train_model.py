import matplotlib.pyplot as plt
import torch
import torch.optim as optim
import numpy as np
import joblib
from klepto.archives import file_archive
from dataset import MyDataset
from noisevocoder import NoiseVocoder
from model import TCN
from mseloss import MSELoss
from pitchloss import PitchLoss

#### HELPER FUNCTIONS ####

def tf_to_np(tensor_song, device):   
    if device.type == 'cuda':
        tensor_song = tensor_song.cpu()
    numpy_song = tensor_song.detach().numpy().ravel()
    return numpy_song

def train_model(model, data, optimizer, scheduler, criterion, n_epochs, device, pitch_difference):
    train_loss_lst = np.zeros((n_epochs))
    val_loss_lst = np.zeros((n_epochs))
    
    trainlength = len(data["train"])
    vallength = len(data["valid"])
    
    train_loader = data["train"]
    val_loader = data["valid"]
    
    train_sr = data["train_sr"]
    valid_sr = data["valid_sr"]
    
    for epoch in range(n_epochs): # loop n_epochs times over the dataset
        train_loss = 0.0
        val_loss = 0.0
        
        ## Training
        model.train()
        
        for (song, sr) in zip(train_loader, train_sr): #dataloader makes it [batch, song_length, song_features]
            
            optimizer.zero_grad()
            
            X_train = song.to(device)
            output = model.forward(X_train) 
            y_pred, y_target = pitch_difference.calc_voc_spec(output, X_train, sr)
            loss = criterion(y_pred, y_target) 
            loss.backward()   
            optimizer.step()
            train_loss += loss.item()
            
        
        train_loss_lst[epoch] = train_loss/trainlength
                   
        ## Validation
        model.eval()
        
        with torch.no_grad():
            for (song, sr) in zip(val_loader, valid_sr):
                
                X_val = song.to(device)
                output = model.forward(X_val)
                y_pred, y_target = pitch_difference.calc_voc_spec(output, X_val, sr)
                loss = criterion(y_pred, y_target)
                val_loss += loss.item()
                
            val_loss_lst[epoch] = (val_loss/vallength)
        
        scheduler.step()
        
        if epoch % 1 == 0:
            print("Epoch {}/{}. Train loss: {:.3f}. Validation loss: {:.3f}.".format(
                1+epoch, n_epochs, train_loss_lst[epoch], val_loss_lst[epoch]))
            
    return val_loss_lst, train_loss_lst       
            

##### LOAD IN DATA ####

data = {}

filename = "music_data"
loaded_model = file_archive(filename)
loaded_model.load()

data["train"] = torch.utils.data.DataLoader(MyDataset(loaded_model["train"], loaded_model["train_sr"], 8), batch_size=5)
data["valid"] = torch.utils.data.DataLoader(MyDataset(loaded_model["valid"], loaded_model["valid_sr"], 8), batch_size=10)
data["test"] = torch.utils.data.DataLoader(MyDataset(loaded_model["test"], loaded_model["test_sr"], 8), batch_size=1)

data["train_sr"] = loaded_model["train_sr"]
data["test_sr"] = loaded_model["test_sr"]
data["valid_sr"] = loaded_model["valid_sr"]


##### TRAIN MODEL ####

n_epochs = 25
center_frequencies = [250, 500, 1000, 2000, 4000, 8000]
vocoder = NoiseVocoder(center_frequencies)

#device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = torch.device('cpu')

tcn = TCN(1, [8, 16, 16, 8], 1, dropout = 0.1)
tcn.to(device)

pitch_difference = PitchLoss(vocoder)

criterion = MSELoss() 

optimizer = optim.Adam(tcn.parameters(), lr = 0.001, weight_decay = 0.99)
scheduler = optim.lr_scheduler.StepLR(optimizer, 10)

val_loss_lst, train_loss_lst = train_model(tcn, data, optimizer, scheduler, criterion, n_epochs, device, pitch_difference)

torch.save(tcn.state_dict(), "model8-16-16-8-drop01.pt")
joblib.dump(val_loss_lst, "validation_loss.sav")
joblib.dump(train_loss_lst, "training_loss.sav")

## Plotting train and validation loss
# x_epochs = np.arange(0, n_epochs)

# plt.plot(x_epochs, train_loss_lst, color = 'red', label = 'Training Loss')
# plt.plot(x_epochs, val_loss_lst, color = 'blue', label = 'Validation Loss')
# plt.xlabel("Epoch")
# plt.ylabel("Loss")
# plt.title("Training and Validation Loss")
# plt.legend()
# plt.show()