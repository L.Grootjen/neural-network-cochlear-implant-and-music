from tools.TCN import TemporalConvNet
import torch.nn as nn

class TCN(nn.Module):
    def __init__(self, n_in, num_channels, n_out, kernel_size = 2, dropout = 0.2):
        super(TCN, self).__init__()
        self.tcn = TemporalConvNet(n_in, num_channels, kernel_size, dropout=dropout)
        self.linear = nn.Linear(num_channels[-1], n_out)

    def forward(self, x):
        # x needs to have dimension (N, C, L) in order to be passed into CNN
        output = self.tcn(x.transpose(1, 2)).transpose(1, 2)
        output = self.linear(output).double()
        return output