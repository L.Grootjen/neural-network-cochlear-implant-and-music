import scipy.signal as sp
import torch
import torch_lfilter
import numpy as np
import torch.fft as torch_fft

class NoiseVocoder():
    
    def __init__(self, center_frequencies, cutoff = 20):
        self.cutoff = cutoff
        self.cf = center_frequencies
        
    def extract_envelope(self, x, fs, order=1):
        envelope = torch.abs(x)
        b, a = sp.butter(order, self.cutoff * 2 / fs)
        a = torch.DoubleTensor(a)
        b = torch.DoubleTensor(b)
        low_pass_filtered_envelope = torch_lfilter.lfilter(b, a, envelope)
        return low_pass_filtered_envelope

    def design_filterbank(self, fs):
        boundaries = [self.third_octave_bounds(cfs) for cfs in self.cf]
        return [self.design_filter(bounds, fs) for bounds in boundaries]

    def third_octave_bounds(self, cfreq):
        third_octave_ratio = 2 ** (1/4)  
        # 2 * 1/3 = 1/9 ### Logaritmische schaal, vandaar **. 1/3 oktaaf is
        # gelijk aan 4 semitones. Literatuur zegt dat ze pas vanaf 7 semitones kunnen horen, dit komt neer op 0.58 oktaaf
        # wat we afronden naar een half oktaaf. Dus dan 2 * 1/4 = 1/2
        return np.array([cfreq / third_octave_ratio, cfreq * third_octave_ratio])
    
    def apply_filterbank(self, x, fs):
        n_samples = x.shape[-1]
        n_channels = len(self.cf)
        coefficients = self.design_filterbank(fs)
        y = torch.empty(size=(n_channels, n_samples))
        for idx, (b, a) in enumerate(coefficients):     
            a = torch.DoubleTensor(a)
            b = torch.DoubleTensor(b)
            test = torch_lfilter.lfilter(b, a, x)
            y[idx] = test
        return y

    def design_filter(self, bounds, fs):
        return sp.butter(4, bounds * 2 / fs, btype='bandpass')

    def vocode_batch(self, x, fs, amplify, add_noise):
        vocoded_songs = torch.empty_like(x)
        batch_size = x.shape[0]
        for batch in range(batch_size):
            voc = self.vocode(x[batch, :], fs, amplify, add_noise)
            vocoded_songs[batch] = voc
        
        return vocoded_songs
        
        
    def vocode(self, x, fs, amplify, add_noise):
        n_channels = len(self.cf)
        n_samples = x.shape[-1]
        noise = torch.rand(n_samples) 
        filtered_x = self.apply_filterbank(x, fs)
        filtered_noise = self.apply_filterbank(noise, fs)
        vocoded_noise = torch.empty((n_channels, n_samples))
        
        if amplify:
            filtered_x_amplified = torch_fft.fft(filtered_x) * 3 #amplify frequencies which are not filtered out
            filtered_x = torch_fft.ifft(filtered_x_amplified)
        
        for idx, (x_band, noise_band) in enumerate(zip(filtered_x, filtered_noise)):
            envelope = self.extract_envelope(x_band, fs)
            if add_noise:
                vocoded_noise[idx] = envelope * noise 
            else: 
                vocoded_noise[idx] = envelope
   
        return torch.sum(vocoded_noise, dim=0)
        