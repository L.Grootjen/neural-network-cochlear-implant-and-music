import torch.nn as nn

class MSELoss(nn.Module):
    
    def __init__(self):
        super(MSELoss, self).__init__()
        self.mse = nn.MSELoss()
    
    def forward(self, y_pred, y_target):
        mse_val = self.mse(y_pred, y_target) 
        return mse_val