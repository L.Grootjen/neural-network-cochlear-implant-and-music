import librosa
import numpy as np
import glob
from klepto.archives import file_archive

data = file_archive('music_data')

path_train_data = glob.glob("./musicnet/train_data/*.wav")
path_test_data = glob.glob("./musicnet/test_data/*.wav")

train_data = []
train_sample_rate = []

test_data = []
test_sample_rate = []
 
for i, file_name in enumerate(path_train_data):
    y, sr = librosa.load(file_name)
    train_data.append(y)
    train_sample_rate.append(sr)
    print(str(i+1) + " out of 320 processed for training data")

for i, file_name in enumerate(path_test_data):
    y, sr = librosa.load(file_name)
    test_data.append(y)
    test_sample_rate.append(sr)
    print(str(i+1) + " out of 10 processed for test data")
    

amount_test = len(test_data)
train_data = train_data[amount_test:]
data["train_sr"] = train_sample_rate[amount_test:]
valid_data = train_data[:amount_test]
data["valid_sr"] = train_sample_rate[:amount_test]
test_data = test_data
data["test_sr"]  = test_sample_rate

data["train"] = train_data
data["test"] = test_data
data["valid"] = valid_data

data.dump()